package ic.app


import ic.ifaces.stoppable.Stoppable
import ic.ifaces.stoppable.ext.stopNonBlocking

import ic.cmd.Console
import ic.cmd.Command


abstract class CommandLineApp : App() {


	protected abstract val command : Command


	override fun implRun (args: String) {
		command.run(
			Console.Standard,
			args
		)
		onCommandFinish()
	}


	protected open fun onCommandFinish() {}


	override fun implStopNonBlocking() {
		val command = command
		if (command is Stoppable) {
			command.stopNonBlocking()
		}
	}


	override fun implWaitFor() {
		val command = command
		if (command is Stoppable) {
			command.waitFor()
		}
	}


}

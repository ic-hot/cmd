package ic.cmd


import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.ext.asInt32
import ic.ifaces.action.between.BetweenAction2
import ic.struct.list.ConvertList
import ic.struct.list.int32range.Int32Range
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.foreach.forEachIndexed
import ic.struct.list.ext.getOrNull
import ic.struct.list.ext.reduce.minmax.maxInt32


fun writeTable (

	console: Console,

	rows : List<List<String>>,

	indentTabs: Int

) {

	val columnMaxLengths = ConvertList(
		Int32Range(
			rows.maxInt32(or = 0) { it.length.asInt32 }
		)
	) { _, columnIndex ->
		rows.maxInt32 { row ->
			row.getOrNull(columnIndex.asInt64)?.length ?: 0
		}
	}

	rows.forEach { row ->

		repeat(indentTabs) { console.write("    ") }

		row.forEachIndexed(
			BetweenAction2(
				run = { columnIndex, string ->
					console.write(string)
					repeat(columnMaxLengths[columnIndex] - string.length) {
						console.putCharacter(' ')
					}
				},
				runInBetween = {
					console.putCharacter(' ')
				}
			)
		)

		console.writeNewLine()

	}

}
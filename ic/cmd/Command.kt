package ic.cmd


import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.MessageException
import ic.app.res.funs.getResString


abstract class Command {


	/**
	 * For example "commit"
	 */
	abstract val name: String

	/**
	 * For example "commit -m \"<message>\""
	 */
	abstract val syntax: String

	abstract val description: String


	@Throws(InvalidSyntaxException::class, MessageException::class)
	protected abstract fun implementRun (console: Console, args: String)

	fun run (console: Console, args: String) {
		try {
			implementRun(console, args)
		} catch (invalidSyntax: InvalidSyntaxException) {
			console.writeLine(
				getResString("cmd/invalid-syntax")
				.replace("$(actual)", "$name $args")
				.replace("$(correct)", syntax)
			)
		} catch (e: MessageException) {
			console.writeLine(e.message)
		}
	}


	fun run (console: Console) = run(console, "")


}

package ic.cmd


import ic.struct.list.ConvertList
import ic.struct.list.List


internal fun writeCommandList (console: Console, commands: List<Command>) {

	console.writeLine("Available commands:")

	writeTable(
		console,
		ConvertList (commands) { _, command ->
			List(
				command.syntax,
				"-",
				command.description
			)
		},
		if (console.toShortenIndentation) 0 else 1
	)

}
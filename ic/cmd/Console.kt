package ic.cmd


import ic.base.primitives.character.Character
import ic.base.throwables.End
import ic.ifaces.lifecycle.closeable.Closeable
import ic.text.TextOutput
import ic.text.input.TextInput


interface Console : TextOutput, TextInput, Closeable {

	
	val toShortenIndentation : Boolean get() = false


	object Standard : Console {

		@Synchronized
		override fun putCharacter (character: Character) = print(character)

		private sealed class ReadState {

			object Initial : ReadState()

			class Line (
				var line : String
			) : ReadState() {
				var index : Int = 0
			}

			object EndOfLine : ReadState()

		}

		private var readState : ReadState = ReadState.Initial

		@Throws(End::class)
		@Synchronized
		@Suppress("FoldInitializerAndIfToElvis")
		override fun getNextCharacterOrThrowEnd() : Character {
			when (val state = readState) {
				is ReadState.Initial -> {
					val line = readlnOrNull()
					if (line == null) throw End
					if (line.isEmpty()) return '\n'
					else {
						readState = ReadState.Line(line)
						return getNextCharacterOrThrowEnd()
					}
				}
				is ReadState.Line -> {
					val char = state.line[state.index++]
					if (state.index >= state.line.length) {
						readState = ReadState.EndOfLine
					}
					return char
				}
				is ReadState.EndOfLine -> {
					readState = ReadState.Initial
					return '\n'
				}
			}
		}

		override fun close() {}

	}


}
package ic.cmd


import ic.base.loop.loop
import ic.struct.list.List
import ic.base.throwables.NotExistsException
import ic.parallel.funs.sleep
import ic.struct.list.ext.reduce.find.findOrThrow
import ic.system.funs.getCurrentEpochTimeMs


internal fun runShell (

	console : Console,

	children : List<Command>,

	shellWelcome 	: String?,
	shellTitle 		: String?

) {

	if (shellWelcome != null) console.writeLine(shellWelcome)

	var commandStartTime = 0L

	loop {

		if (getCurrentEpochTimeMs() - commandStartTime > 16384L) {

			if (shellTitle != null) console.writeLine(shellTitle)

			writeCommandList(console, children)

		}

		sleep(256)

		console.write(": ")

		val input = console.readLineAsText()

		if (input.equals("exit")) return

		commandStartTime = getCurrentEpochTimeMs()

		val inputIterator = input.newIterator()

		val commandName = inputIterator.readTill(' ').toString()

		val args = inputIterator.read()

		if (commandName.isEmpty()) {

			if (args.isEmpty) {

				if (shellTitle != null) console.writeLine(shellTitle)

				writeCommandList(console, children)

			} else {

				console.writeLine("Command name can't be empty.")

				writeCommandList(console, children)

			}

		} else {

			val command = try {

				children.findOrThrow { it.name == commandName }

			} catch (notExists: NotExistsException) {

				console.writeLine("No such command \"$commandName\".")
				writeCommandList(console, children)
				return

			}

			command.run(console, args.toString())

		}

	}

}
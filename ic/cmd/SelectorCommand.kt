package ic.cmd


import ic.base.strings.ext.asText
import ic.struct.list.List
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.MessageException
import ic.base.throwables.NotExistsException
import ic.ifaces.stoppable.Stoppable
import ic.struct.list.ext.reduce.find.findOrThrow


abstract class SelectorCommand : Command(), Stoppable {


	protected abstract val shellWelcome	: String?
	protected abstract val shellTitle	: String

	protected abstract val children : List<Command>


	private var runningCommand : Command? = null


	@Throws(InvalidSyntaxException::class, MessageException::class)
	override fun implementRun (console: Console, args: String) {

		val argsIterator = args.asText.newIterator()

		val commandName = argsIterator.readTill(' ')
		val commandArgs = argsIterator.read()

		if (commandName.isEmpty) {

			if (commandArgs.isEmpty) {

				val children = children
				val shellWelcome = shellWelcome
				val shellTitle = shellTitle

				runShell(
					console = console,
					children = children,
					shellWelcome = shellWelcome,
					shellTitle = shellTitle
				)

			} else {

				console.writeLine("Command name can't be empty.")

				writeCommandList(console, children)

			}

		} else {

			try {

				val command = children.findOrThrow {
					it.name == commandName.toString()
				}

				synchronized(this) {
					runningCommand = command
				}

				command.run(console, commandArgs.toString())

			} catch (notExists: NotExistsException) {

				console.writeLine("No such command \"$commandName\".")
				writeCommandList(console, children)
				return

			} finally {

				synchronized(this) {
					runningCommand = null
				}

			}

		}

	}


	override fun stopNonBlockingOrThrowNotNeeded() {

		val runningCommand = synchronized(this) {
			val runningCommand = this.runningCommand
			this.runningCommand = null
			runningCommand
		}

		if (runningCommand is Stoppable) {
			runningCommand.stopNonBlockingOrThrowNotNeeded()
		}

	}

	override fun waitFor() {

		val runningCommand = synchronized(this) {
			val runningCommand = this.runningCommand
			this.runningCommand = null
			runningCommand
		}

		if (runningCommand is Stoppable) {
			runningCommand.waitFor()
		}

	}


}
